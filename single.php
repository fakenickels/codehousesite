<?php 
/**
 * Pagina do post 
 *
 * @author Codehouse
 * @subpackage template 
 */ 


// buscando as infos do POST 
the_post();

/* 
    # Exemplos de utilização 

    // CSS, JS e headers adicionais 
    global $pagina;
    $pagina['css'][] = 'single.css';
    $pagina['js'][]  = TEMPLATE . 'js/pages/single.js';

    // Pode se setar tags Open Graph, por exemplo 
    $pagina['header'][] = '<meta property="og:title" content="<?php the_title(); ?>" />';
*/ 

get_header() ?>

	<div id='posts' class='container-fluid'>
		<div class='row-fluid'>
		<?php
			$post = get_post( $_GET['p'] );
			if( $post ) {
		?>
			<div class='span4'>
				<h2>
					<a href="<?php the_permalink() ?>">
						<?php echo $post->post_title ?>
					</a> 	
					<p class='lead'>By Codehouse às <?php echo $post->post_date ?></p>
				</h2>
				<div class='post-content'><?php echo $post->post_content ?></div>		
				<div id="disqus_thread"></div>
				<script type="text/javascript">
					/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
					var disqus_shortname = 'codehouse'; // required: replace example with your forum shortname

					/* * * DON'T EDIT BELOW THIS LINE * * */
					(function() {
						var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
						dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
						(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
					})();
				</script>
				<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
				<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>	
			</div>
			<?php } else { ?>
			<div class='span4'>
				<h2>Bee boop! 404!</h2>
				<p>Izto non ecziste! É só mais uma imaginação da sua cabeça para tentar ti fazer feliz.</p>
				<a href="/">Homar?</a>
			</div>
			<?php } ?>
		</div>
	</div>
<?php
	get_footer();
?>