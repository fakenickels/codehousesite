<?php 
/**
 * Pagina inicial do template 
 *
 * @author Codehouse 
 * @subpackage template 
 */ 


// header 
get_header();
?>	

<div class="container">
	<header class="header">
		
	</header><!-- .header --> 

	<section class="content">

<?php
// posts 
while( have_posts() ) : 
	the_post();
	?>
	<div class="post">
		<div class="post-title">
			<h2>
				<a href="<?php the_permalink() ?>">
					<?php the_title(); ?>
				</a>
			</h2>
		</div><!-- .post-title --> 

		<div class="post-content">
			<?php 
			// resumo do conteudo (http://codex.wordpress.org/Function_Reference/the_excerpt)
			the_excerpt();
			?> 
		</div><!-- .post-content --> 
	</div><!-- .post --> 
	<?php 
endwhile;


// footer 
get_footer();

