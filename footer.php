<?php 
/**
 * Footer do seu template WP 
 *
 * @author Codehouse 
 * @subpackage template 
 */ 
?>
    <div id='rodape' class='container-fluid'>
        <div class='row-fluid'>
            <div class='span3 offset1 infos'>
                <ul>
                    <li><img src='<?php echo TEMPLATE ?>img/phone-icon.png'/> (86)9916-9848</li>
                    <li><img src='<?php echo TEMPLATE ?>img/face-icon.png'/> facebook.com/codehouseco</li>
                    <li><img src='<?php echo TEMPLATE ?>img/mail-icon.png'/> equipe@codehouseco.com</li>
                </ul>
            </div>
            <div class='span3'>
                <blockquote>Copyright &copy; CodeHouse 2013<br/>Todos os direitos reservados</blockquote>
            </div>
        </div>
    </div>

    <!-- jQuery --> 
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript">window.jQuery || document.write('<script src="<?php echo TEMPLATE ?>js/libs/jquery.js"><\/script>')</script> 
    <script type="text/javascript" src="<?=TEMPLATE?>js/pages/script.js"></script>

    <?php 
    // buscando os JS adicionais 
    global $pagina;
    foreach( $pagina['js'] as $js ) : 
        ?>
        <script type="text/javascript" src="<?=$js?>"></script>
        <?php 
    endforeach;
    ?>

    <script type="text/javascript">
    // analytics 
    var _gaq=[['_setAccount','<?=GA?>'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));

    <?php 
    // conversões no GA 
    foreach( $pagina['analytics'] as $conversao )
    {
        echo $conversao;
    }
    ?>
    </script>
	
	<script type='text/javascript'>
	$(function(){
		$('#contact-form').submit(function(e){
			e.preventDefault();
			$('p#form-status').innerText('Dados enviados!').fadeIn();
		});
	});
	</script>
</body>
</html>