<?php
	get_header();
if ( $_SERVER['REQUEST_METHOD'] == "POST" && $_POST['ismeeting'] == 'form-msg'){
     //envia o form
     send_contact_form();            
 
}
?>	
<?php if(!isset( $_GET['posts'] )) {  ?>	
	<div id='main-slid' class='row-fluid'>
		<div id='slide-carousel' class='carousel slide'>
			<div class='carousel-inner'>
				<div class='active item site'>
					<div class='row-fluid'>
						<div class='span3 offset3'>
							<img src='<?php echo TEMPLATE ?>img/chave.png'/>
						</div>
						<div class='span5'>
							<h1 class='title'>Criação<br/> de Sites</h1>
						</div>				
					</div>
					<div class='row-fluid'>
						<div class='span8 offset2'>
							<p class='lead'>Você quer um site moderno, bonito, responsivo, de alta performance e de preço acessível?</p>
						</div>
					</div>
				</div>
				<div class='item design'>
					<div class='row-fluid'>
						<div class='span3 offset3'>
							<img src='<?php echo TEMPLATE ?>img/olho.png'/>
						</div>
						<div class='span5'>
							<h1 class='title'>Comunicação Visual</h1>
						</div>
					</div>
					<div class='row-fluid'>
						<div class='span8 offset2'>
							<p class='lead'>Precisa de uma arte para qualquer serviço gráfico com alta qualidade e sofisticação?</p>
						</div>
					</div>
				</div>
				<div class='item solucao'>
					<div class='row-fluid'>
						<div class='span3 offset3'>
							<img src='<?php echo TEMPLATE ?>img/lamp.png'/>
						</div>
						<div class='span5'>
							<h1 class='title'>Somos<br/> a sua <br/>Solução</h1>
						</div>
					</div>
					<div class='row-fluid'>
						<div class='span8 offset2'>
							<p class='lead'>Somos mais que uma empresa. Somos uma ideia, uma convicção. Seu projeto será a nossa paixão</p>
						</div>
					</div>
				</div>
				<!--<div class='item solucao'>
					<div class='row-fluid'>
						<div class='span3 offset3'>
							<img src='<?php echo TEMPLATE ?>img/lamp.png'/>
						</div>
						<div class='span5'>
							<h1 class='title'>Somos<br/> a sua <br/>solução</h1>
						</div>
					</div>
					<div class='row-fluid'>
						<div class='span8 offset2'>
							<p class='lead'>Somos mais que uma empresa. Somos uma ideia, uma convicção. Seu projeto será a nossa paixão</p>
						</div>
					</div>
				</div>-->

			</div>
			<a class='carousel-control left' href='#slide-carousel' data-slide='prev'>&lsaquo;</a>
			<a class='carousel-control right' href='#slide-carousel' data-slide='next'>&rsaquo;</a>
		</div>
	</div>

	<div id='home' class='container-fluid'>
		<div id='main-artg' class='row-fluid'>
			<div class='span4 offset4'>
				<img src='<?php echo TEMPLATE ?>img/cool.png'/>
			</div>
		</div>
	</div>
	<div id='equipe' class='container-fluid'>
		<div class='mega-title row-fluid'>
			<div class='span12'>
				<div class='span4 offset4'>
					<h1 style='cursor:pointer'>A Equipe</h1>
				</div>
			</div>
		</div>
		<div class='row-fluid'>
			<ul class='thumbnails offset2'>
				<li class='span3 widget'>
					<div class='thumbnail'>
						<img src='<?php echo TEMPLATE ?>img/gabriel.jpg' class='img-circle'>
						<div class='caption'>
							<h3>Gabriel Rubens</h3>
							<p>Em algum lugar, algo incrível está esperando para ser descoberto [42]</p>
							<ul>
								<li>
									<a href='http://facebook.com/grubens1'><img src='<?php echo TEMPLATE ?>img/face-icon.png'/> grubens1</a>
								</li>
								<li>
									<a href='http://github.com/grsabreu'>
										<img src='<?php echo TEMPLATE ?>img/github.ico'/> @grsabreu
									</a>
								</li>
								<li>
									<a href='mailto:grsabreu@gmail.com'><img src='<?php echo TEMPLATE ?>img/mail-icon.png'/> grsabreu@gmail.com</a>
								</li>
							</ul>
						</div>
					</div>
			</li>
			<li class='span3 widget'>
				<div class='thumbnail'>
					<img src='<?php echo TEMPLATE ?>img/judson.jpg' class='img-circle'>
					<div class='caption'>
						<h3>Judson Silva</h3>
						<p>Um desenvolvedor, designer, estudante, professor e músico</p>
						<ul>
							<li>
								<a href='http://facebook.com/judson.bs'>
									<img src='<?php echo TEMPLATE ?>img/face-icon.png'/> judson.bs
								</a>
							</li>
							<li>
								<a href='mailto:judsonbarroso@gmail.com'>
									<img src='<?php echo TEMPLATE ?>img/mail-icon.png'/>judsonbarroso@gmail.com
								</a>
							</li>
						</ul>
					</div>
				</div>
			</li>
			<li class='span3 widget'>
				<div class='thumbnail'>
					<img src='<?php echo TEMPLATE ?>img/jafferson.jpg' class='img-circle'>
					<div class='caption'>
						<h3>Jafferson Sousa</h3>
						<p>Pense em mim como o Yoda, mas ao invés de ser pequeno e verde, eu uso terno e sou incrível!</p>
						<ul>
							<li>
								<a href="http://facebook.com/jaffersonsousa">	
									<img src='<?php echo TEMPLATE ?>img/face-icon.png'/>
									jaffersonsousa
								</a>
							</li>
							<li>
								<a href='mailto:jafferson20@gmail.com'>
									<img src='<?php echo TEMPLATE ?>img/mail-icon.png'/> jafferson20@gmail.com
								</a>
							</li>
						</ul>
					</div>
				</div>
			  </li>
			</ul>
		</div>
	</div>
	<div id='portfolio' class='container-fluid'>
		<div class='mega-title row-fluid'>
			<div class='span12'>
				<div class='span4 offset4'>
					<h1>Portfólio</h1>
				</div>
			</div>
		</div>
		<img src='<?php echo TEMPLATE ?>img/loader.gif'/><br/>
		<p>Em breve...</p>
	</div>
<?php } else { ?>
	<div id='posts' class='container-fluid'>
		<div class='row-fluid offset1'>
		<?php if( $_GET['posts'] == 'all' ){ ?>
			<?php while( have_posts() ){ 
				the_post();
			?>
			<div class='span4'>
				<h2>
					<a href="<?php the_permalink() ?>">
						<?php the_title(); ?>
					</a>
				</h2>
				<div class='post-content'><?php the_excerpt(); ?></div>
				<p><a href="<?php the_permalink() ?>">Ler >></a></p>				
			</div>
			<?php } // endwhile ?>
		<?php } else {
			$post = get_post( $_GET['posts'] );
			if( $post ) {
		?>
			<div class='span4'>
				<h2>
					<a href="<?php the_permalink() ?>">
						<?php echo $post->post_title ?>
					</a>
					<p class='lead'>By Codehouse às <?php echo $post->post_date ?></p>
				</h2>
				<div class='post-content'><?php echo $post->post_content ?></div>		
			</div>
			<?php } else { ?>
			<div class='span4'>
				<h2>Bee boop! 404!</h2>
				<p>Izto non ecziste, é só imaginação da sua cabeça.</p>
				<a href="/">Homar?</a>
			</div>
			<?php } ?>			
		<?php } ?>
		</div>
	</div>
<?php } // endif ?>

	<div id='contato' class='container-fluid'>
		<div class='row-fluid'>
			<form class='form-horizontal span6 offset3' method='post' action='http://codehouseco.com/'>
				<input type='hidden' value='form-msg' name='ismeeting'/>
				<div class='mega-title row-fluid'>
					<div class='span12'>
						<h1>Contato</h1>
					</div>
				</div>
				<div class='control-group'>
					<input type='text' class='div2' placeholder='Nome' name='nome' required/>
					<input type='text' class='div2' placeholder='Email' name='email' required/>
				</div>
				<div class='control-group'>
					<textarea class='div1' placeholder='Mensagem' name='msg' required></textarea>
				</div>
				<div class='control-group'>
					<button type='submit' class='btn-large'>Enviar</button>
				</div>
			</form>
		</div>
	</div>
		
<?php
	get_footer();
?>