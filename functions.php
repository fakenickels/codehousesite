<?php 
/**
 * Arquivo de funções do tema NOME DO TEMA 
 *
 * @author Codehouse 
 * @subpackage template 
 */ 



/**
 * Definindo constantes para nao ficar utilizando a função get_bloginfo() a toda hora
 * Ajuda na performance 
 */ 
define('URL', get_bloginfo('url') . '/');
define('TEMPLATE', get_bloginfo('template_url') . '/');


/**
 * O código do seu google analytics 
 */ 
define('GA', 'UA-42929912-1');



/**
 * Variavel global pagina 
 * Utilizada para carregar CSS e JS específicos da paginas internas 
 * Ver exemplo no arquivo single.php 
 */ 
global $pagina;
$pagina['css']       = array(); # utilizado para CSS especificos de internas 
$pagina['js']        = array(); # utilizado para novos arquivos JS 
$pagina['header']    = array(); # utilizado pra novas meta tags ou algo do tipo no header 
$pagina['analytics'] = array(); # utilizado para marcar conversões no GA 

$pagina['js'][] = TEMPLATE . 'js/libs/bootstrap.min.js';

/**
 * Suporte para imagens destacas no post 
 */ 
add_theme_support('post-thumbnails');


/**
 * Registrando a primeira sidebar 
 */ 
register_sidebar( array(
    'name'          => 'Sidebar 1', 
    'id'            => 'sidebar-1', 
    'description'   => 'Conteúdo da sidebar', 
    'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">', 
    'after_widget'  => '</div>', 
    'before_title'  => '<h3 class="sidebar-widget-title">', 
    'after_title'   => '</h3>'
) );



/**
 * Removendo o WP generator 
 */ 
remove_action('wp_head', 'wp_generator');

function send_contact_form(){        
 
    $siteurl = trailingslashit(get_option('home'));
    $mailto = 'equipe@codehouseco.com';
    $subject = get_option('blogname'). ' - Yip yahoo! Um cliente!';
    $headers = 'From: ' . get_option('blogname') . ' <'. $mailto .'>' . "\r\n";
    $headers.= 'Reply-To: '.$_POST['email']. "\r\n";
 
    $message  = 'Senhores,' . "\r\n\r\n";
    $message .= 'Temos uma nova mensagem recebida do site às ' .date("d/m/Y \à\s H:i:s"). "\r\n\r\n";
    $message .= 'MENSAGEM' . "\r\n";
    $message .= '-----------------------' . "\r\n";
 
    while(list($campo, $valor) = each($_POST)){
        if($campo != "submit"){
 
            $message.= ucfirst($campo) .":  ". $valor . "\r\n\r\n";
        }
 
    }    
 
    $message .= '-----------------------' . "\r\n\r\n";
    $message .= 'Atenciosamente,' . "\r\n";
    $message .= get_option('blogname') . "\r\n";
    $message .= $siteurl . "\r\n\r\n\r\n\r\n";
 
    // ok let's send the email
    if( !wp_mail($mailto, $subject, $message, $headers) ){
        echo '<div class="aviso error"><p>A mensagem não pôde ser enviada. Por favor, tente novamente.</p></div>';
    } else {
        echo '<div class="aviso success"><p>Mensagem enviada com sucesso!</p></div>';
    }
 
}
?>