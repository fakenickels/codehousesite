<?php 
/** 
 *
 * @author Codehouse
 * @subpackage template 
 */ 
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class='no-js lt-ie9 lt-ie8 lt-ie7' lang='en'> <![endif]-->
<!--[if IE 7]>    <html class='no-js lt-ie9 lt-ie8' lang='en'> <![endif]-->
<!--[if IE 8]>    <html class='no-js lt-ie9' <?php language_attributes('html') ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class='no-js' <?php language_attributes('html') ?>> <!--<![endif]-->
<head>
	<meta charset='utf-8' />
	<meta name="robots" content="index,follow">
	<meta name="keywords" content="sites, javascript, html5, css, css3,desenvolver site, projeto web, seo, quero um site, como fazer um site" />
	<meta name="description" content="Codehouse, soluções em WEB e design" />
	<meta name="google-site-verification" content="fIZnA7CLruu4SMRkoufFaM0s2a5FSjPcXE42VtKrY7Q" />
	
	<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
	<meta property='og:title' content='Codehouse | Soluções em WEB e design'/>
	<meta property='og:site_name' content='Codehouse'/>
	<meta property='og:type' content='site'/> 
	<meta property='og:image' content='<?php echo TEMPLATE ?>img/facebookico.png'/>

	<title><?php bloginfo('name') ?> | Soluções em WEB e design</title>
	
	<?php 
	// header do wordpress 
	wp_head();

	// buscando os headers adicionais 
	global $pagina;
	foreach( $pagina['header'] as $js ) : 
		?>
		<script type='text/javascript' src='<?php echo $js?>'></script>
		<?php 
	endforeach;
	?>

	<meta name='viewport' content='width=device-width, initial-scale=1.0'> 
	<link rel='stylesheet' href='<?php echo TEMPLATE ?>css/css.php?v=1&amp;f=<?php foreach( $pagina['css'] as $css ) echo $css . ','; ?>' /> 
	<link rel='shortcut icon' href='<?php echo TEMPLATE ?>img/chico.ico'/>
</head>
<body>
<div id='head-top' class='container'>
	<div class='row-fluid'>
		<div class='span6' id='logo'>
			<img src='<?php echo TEMPLATE ?>img/banner.png'/>
		</div>
		<div class='span6' class='menu-top'>
			<div class='navbar'>
				<div id='main-menu' class='navbar-inner'>
					<ul class='nav'>
						<li><a href='/#head-top'>Início</a></li>
						<li><a href='/#equipe'>A Equipe</a></li>
						<li><a href='/?posts=all'>Blog</a></li>
						<li><a href='/#portfolio'>Portfólio</a></li>
						<li><a href='/#contato'>Contato</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>